package com.ivarrace.springsecurityjwt.security.services;

import com.ivarrace.springsecurityjwt.model.Role;
import com.ivarrace.springsecurityjwt.model.RoleName;
import com.ivarrace.springsecurityjwt.model.User;
import com.ivarrace.springsecurityjwt.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class UserDetailsServiceImplTest {

    @InjectMocks
    UserDetailsServiceImpl userDetailsService = new UserDetailsServiceImpl();

    @Mock
    private UserRepository userRepository;

    @Test
    void loadUserByUsername_found() {
        //Given
        User user = new User("name", "username", "email", "password");
        user.setId(42L);
        Set<Role> roles = new HashSet<>();
        Role userRole = new Role(RoleName.ROLE_USER);
        roles.add(userRole);
        user.setRoles(roles);
        //When
        when(userRepository.findByUsername("username")).thenReturn(Optional.of(user));
        UserPrinciple response = (UserPrinciple) userDetailsService.loadUserByUsername(user.getUsername());
        //Then
        assertEquals(user.getId(), response.getId(), "Unexpected Id");
        assertEquals(user.getName(),response.getName(), "Unexpected Name");
        assertEquals(user.getUsername(), response.getUsername(), "Unexpected Username");
        assertEquals(user.getEmail(), response.getEmail(), "Unexpected Email");
        assertEquals(user.getPassword(), response.getPassword(), "Unexpected Password");
        assertEquals(1 , response.getAuthorities().size(), "Unexpected Authorities");
        assertTrue(response.isAccountNonExpired(), "Unexpected value");
        assertTrue(response.isAccountNonLocked(), "Unexpected value");
        assertTrue(response.isCredentialsNonExpired(), "Unexpected value");
        assertTrue(response.isEnabled(), "Unexpected value");
    }

    @Test
    void loadUserByUsername_not_found() {
        when(userRepository.findByUsername("test")).thenReturn(Optional.empty());
        Assertions.assertThrows(UsernameNotFoundException.class, () -> {
            userDetailsService.loadUserByUsername("test");
        }, "Exception expected");
    }
}