package com.ivarrace.springsecurityjwt.controller;

import com.ivarrace.springsecurityjwt.security.jwt.JwtAuthTokenFilter;
import com.ivarrace.springsecurityjwt.security.services.UserPrinciple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestRestAPIs {

    @Autowired
    JwtAuthTokenFilter jwtAuthTokenFilter;

    @GetMapping("/api/test/user")
    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    public String userAccess() {
        UserPrinciple logedUser = (UserPrinciple) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return ">>> User Contents! Logged: " + logedUser.getUsername();
    }

    @GetMapping("/api/test/pm")
    @PreAuthorize("hasRole('PM') or hasRole('ADMIN')")
    public String projectManagementAccess() {
        return ">>> Board Management Project";
    }

    @GetMapping("/api/test/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return ">>> Admin Contents";
    }
}