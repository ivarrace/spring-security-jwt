package com.ivarrace.springsecurityjwt.repository;

import java.util.Optional;

import com.ivarrace.springsecurityjwt.model.Role;
import com.ivarrace.springsecurityjwt.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}